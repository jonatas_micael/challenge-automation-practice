Para configurar este projeto de teste em sua máquina, é necessário:
•	Ter instalado o JDK. podendo ser baixado em:
    Para 32 bits: https://edelivery.oracle.com/akam/otn/java/jdk/8u261-b12/a4634525489241b9a9e1aa73d9e118e6/jdk-8u261-windows-i586.exe 
    Para 64 bits: https://edelivery.oracle.com/akam/otn/java/jdk/8u261-b12/a4634525489241b9a9e1aa73d9e118e6/jdk-8u261-windows-x64.exe 

•	Ter instalado uma IDE de preferência que aceite programação Java (Eclipse, NetBeans, IntelliJ IDEA, ...).
•	Ter instalado o plugin do gerenciador de dependências gradle na IDE
•	Ter instalado o Navegador Google Chrome
•	Ter baixado o ChromeDriver compatível com a versão atual do GoogleChrome disponível em: "https://chromedriver.chromium.org/downloads"

Para instalar e executar este projeto de teste em sua máquina, é necessário:

•	Clonar este repositório
•	Inserir no repositório "src/test/resources" o chromeDriver baixado anteriormente
•	Atualizar as dependências pelo gradle, sendo estas: 
'org.apache.commons:commons-math3:3.6.1' 
'com.google.guava:guava:28.2-jre' 
'junit:junit:4.12' 
'org.seleniumhq.selenium', name: 'selenium-java', version: '3.141.59' 
('/src/test/resources/libs/dataproviders.jar')

E está livre para executar o teste =D.

