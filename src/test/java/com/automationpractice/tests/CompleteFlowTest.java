package com.automationpractice.tests;

import java.util.Random;

import org.junit.Test;

import com.automationpractice.pages.AddressesPage;
import com.automationpractice.pages.AuthenticationPage;
import com.automationpractice.pages.MainPage;
import com.automationpractice.pages.OrderConfirmation;
import com.automationpractice.pages.PaymentPage;
import com.automationpractice.pages.RegisterPage;
import com.automationpractice.pages.ShippingPage;
import com.automationpractice.pages.ShoppingCartPage;
import com.automationpractice.util.Item;
import com.automationpractice.util.PaymentForms;
import com.automationpractice.util.Report;

import generators.peopleGenerator.PeopleGenerator;
import generators.peopleGenerator.Person;

//import generators.personGenerator.People;
//import generators.personGenerator.PeopleGenerator;

public class CompleteFlowTest extends BaseTest {
	
	//Caso de teste: Realizar uma compra com sucesso
	//	Acessar o site: www.automationpractice.com.
	//	Escolha um produto qualquer na loja.
	//	Adicione o produto escolhido ao carrinho.
	//	Prossiga para o checkout.
	//	Valide se o produto foi corretamente adicionado ao
	//	carrinho e prossiga caso esteja tudo certo.
	//	Realize o cadastro do cliente preenchendo todos os
	//	campos obrigat�rios dos formul�rios.
	//	Valide se o endere�o est� correto e prossiga.
	//	Aceite os termos de servi�o e prossiga.
	//	Valide o valor total da compra.
	//	Selecione um m�todo de pagamento e prossiga.
	//	Confirme a compra e valide se foi finalizada com sucesso.
	
	@Test
	public void BuyAnItemWithSuccessTest() {
		testName = "BuyAnItemWithSuccessTest";
		Report.logTestName(testName);
		
		Item selectedItem = Item.values()[new Random().nextInt(Item.values().length)];
		PaymentForms payment = PaymentForms.values()[new Random().nextInt(PaymentForms.values().length)];
		Person person = PeopleGenerator.getNewPerson();
				
		new MainPage(driver)			
			.addItemToCart(selectedItem)
			.proceedToCheckout(ShoppingCartPage.class)
			.validateItemData(selectedItem)
			.proceedToCheckout(AuthenticationPage.class)
			.setEmailField(person.getEmail().getFullEmail())
			.clickInCreateAccount(RegisterPage.class)
			.setSex(person.getSex())
			.setFirstName(person.getName().getFirstName())
			.setLastName(person.getName().getLastName())
			.setPassword(person.getEmail().getPassword())
			.setDayOfMonth(person.getDateOfBirth().getDayOfMonth())
			.setMonth(person.getDateOfBirth().getMonth())
			.setYear(person.getDateOfBirth().getYear())
			.setAddressName(person.getAddress().getAddressName())
			.setCity(person.getAddress().getCity())
			.setState(person.getAddress().getState())
			.setZipCode(person.getAddress().getZipCode())
			.setCountry(person.getAddress().getCountry())			
			.setNumberPhone(person.getNumberPhone())
			.clickInRegisterButton(AddressesPage.class)
			.validateAddress(person)
			.proceedToCheckout(ShippingPage.class)
			.clickInAgreeTerms()
			.validateDeliveryPrice(selectedItem)
			.proceedToCheckout(PaymentPage.class)
			.validateTotalValues(selectedItem)
			.payBy(payment)
			.validateOrderSummary(selectedItem)
			.confirmOrder(OrderConfirmation.class)
			.validateOrderConfirmation(selectedItem);		
			
	}

}
