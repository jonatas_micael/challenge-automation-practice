package com.automationpractice.tests;

import java.time.LocalDate;
import java.util.Calendar;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.automationpractice.util.Report;

public abstract class BaseTest {
		
	protected WebDriver driver;
	public static String testName = "";
	
	@BeforeClass
	public static void setProperties() {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\src\\test\\resources\\chromedriver.exe");
		Report.setExecutionID(LocalDate.now().toString() + "-" + Calendar.getInstance().getTimeInMillis());
	}
	
    @Before 
    public void initiate() {
    	driver = new ChromeDriver();
    	driver.get(getActualUrl());
    	Report.setDriver(driver);
    }
    
    private String getActualUrl() {
    	return "http://automationpractice.com/index.php?";
	}

	@After
    public void finish() {		
    	driver.quit();
    	Report.log("End test\n");
    }      
}
