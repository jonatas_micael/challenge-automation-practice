package com.automationpractice.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.automationpractice.util.Report;
import com.automationpractice.util.Status;

public class AuthenticationPage extends BasePage {

	private By emailField = By.id("email_create");
	private By createAccount = By.id("SubmitCreate");	
	private By footer = By.id("footer");
	
	public AuthenticationPage(WebDriver driver) {
		super(driver);
		loadPage();
	}

	@Override
	protected void loadPage() {		
		wait(ExpectedConditions.visibilityOf(find(createAccount)), DEFAULT_TIMEOUT);
		wait(ExpectedConditions.visibilityOf(find(footer)), DEFAULT_TIMEOUT);		
	}

	public <T extends BasePage> T clickInCreateAccount(Class<T> clazz) {		
		T page = null;
		try {			
			click(createAccount);
			Thread.sleep(SHORT_TIMEOUT * 1000);
			page = createPage(clazz);
			Report.logMethod("clickInCreateAccount", Status.SUCCESSFUL);
		}catch (Throwable e) {
			Report.logMethod("clickInCreateAccount", Status.FAILED, e.getMessage());
		}
		return page;
	}

	public AuthenticationPage setEmailField(String string) {			
		try {			
			write(emailField, string);
			Report.logMethod("setEmailField", Status.SUCCESSFUL);
		}catch (Throwable e) {
			Report.logMethod("setEmailField", Status.FAILED, e.getMessage());
			throw e;
		}
		return this;
	}

}
