package com.automationpractice.pages;

import java.time.Month;
import java.util.Arrays;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.automationpractice.tests.BaseTest;
import com.automationpractice.util.Report;
import com.automationpractice.util.Status;
import com.google.common.base.Strings;

import generators.addressGenerator.Country;
import generators.peopleGenerator.Sex;

//import generators.addressGenerator.Country;
//import generators.peopleGenerator.Sex;

public class RegisterPage extends BasePage {

	private By sexRadio = By.cssSelector("input[type=radio]");
	private By firstname = By.id("customer_firstname");
	private By lastname = By.id("customer_lastname");
	private By email = By.id("email");
	private By password = By.id("passwd");
	private By day = By.id("days");
	private By days = By.cssSelector("#days option");
	private By month = By.id("months");
	private By months = By.cssSelector("#months option");
	private By year = By.id("years");
	private By years = By.cssSelector("#years option");
	private By newsletter = By.id("newsletter");
	private By offer = By.id("optin");
	private By firstnameAddress = By.id("firstname");
	private By lastnameAddress = By.id("lastname");
	private By company = By.id("company");
	private By firstAddress = By.id("address1");
	private By secondAddress = By.id("address2");
	private By city = By.id("city");
	private By state = By.id("id_state");
	private By states = By.cssSelector("#id_state option");
	private By postcode = By.id("postcode");
	private By country = By.id("id_country");
	private By countries = By.cssSelector("#id_country option");
	private By other = By.id("other");
	private By phone = By.id("phone");
	private By mobilePhone = By.id("phone_mobile");	
	private By footer = By.id("footer");
	private By register = By.id("submitAccount");
	
	public RegisterPage(WebDriver driver) {
		super(driver);
		loadPage();
	}

	@Override
	protected void loadPage() {
		wait(ExpectedConditions.visibilityOf(find(register)), DEFAULT_TIMEOUT);	
		try {
			Thread.sleep(SHORT_TIMEOUT);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public RegisterPage setSex(Sex sex) {
		try {
			findElements(sexRadio).get(sex.ordinal()).click();		
			Report.logMethod("setSex", Status.SUCCESSFUL);
		}catch (Throwable e) {
			Report.logMethod("setSex", Status.FAILED, e.getMessage());
			throw e;
		}
		return this;
	}

	public RegisterPage setFirstName(String text) {
		try {
			write(firstname, text);		
			Report.logMethod("setFirstName", Status.SUCCESSFUL);
		}catch (Throwable e) {
			Report.logMethod("setFirstName", Status.FAILED, e.getMessage());
			throw e;
		}
		return this;
	}

	public RegisterPage setLastName(String text) {
		try {
			write(lastname, text);			
			Report.logMethod("setLastName", Status.SUCCESSFUL);
		}catch (Throwable e) {
			Report.logMethod("setLastName", Status.FAILED, e.getMessage());
			throw e;
		}
		return this;
	}

	public RegisterPage setFullEmail(String text) {
		try {
			write(email, text);		
			Report.logMethod("setFullEmail", Status.SUCCESSFUL);
		}catch (Throwable e) {
			Report.logMethod("setFullEmail", Status.FAILED, e.getMessage());
			throw e;
		}
		return this;
	}

	public RegisterPage setPassword(String text) {
		try {
			write(password, text);	
			Report.logMethod("setPassword", Status.SUCCESSFUL);
		}catch (Throwable e) {
			Report.logMethod("setPassword", Status.FAILED, e.getMessage());
			throw e;
		}
		return this;
	}
	
	public RegisterPage setDayOfMonth(int i) {
		try {
			click(day);
			findElements(days).forEach(element -> {
				if (getText(element).trim().equals("" + i)) 
					element.click();			
			});
			Report.logMethod("setDayOfMonth", Status.SUCCESSFUL);
		}catch (Throwable e) {
			Report.logMethod("setDayOfMonth", Status.FAILED, e.getMessage());
			throw e;
		}
		return this;
	}

	public RegisterPage setMonth(Month month) {
		try {
			click(this.month);
			findElements(months).forEach(element -> {
				if (element.getAttribute("innerHTML").replace("&nbsp;", "").trim().equalsIgnoreCase(month.toString())) {
					element.click();
				} 
			});		
			Report.logMethod("setMonth", Status.SUCCESSFUL);
		}catch (Throwable e) {
			Report.logMethod("setMonth", Status.FAILED, e.getMessage());
			throw e;
		}
		return this;
	}

	public RegisterPage setYear(int i) {
		try {
			click(year);
			findElements(years).forEach(element -> {
				if (getText(element).trim().equals("" + i)) 
					element.click();			
			});	
			Report.logMethodAll("setYear", BaseTest.testName, Status.SUCCESSFUL);
		}catch (Throwable e) {
			Report.logMethodAll("setYear", BaseTest.testName, Status.FAILED, e.getMessage());
			throw e;
		}
		return this;
	}

	public RegisterPage setAddressName(String text) {
		try {
			write(firstAddress, text);		
			Report.logMethod("setAddressName", Status.SUCCESSFUL);
		}catch (Throwable e) {
			Report.logMethod("setAddressName", Status.FAILED, e.getMessage());
			throw e;
		}
		return this;
	}

	public RegisterPage setCity(String text) {
		try {
			write(city, text);		
			Report.logMethod("setCity", Status.SUCCESSFUL);
		}catch (Throwable e) {
			Report.logMethod("setCity", Status.FAILED, e.getMessage());
			throw e;
		}
		return this;
	}

	public RegisterPage setState(String text) {
		try {
			click(state);
			findElements(states).forEach(element -> {
				if (element.getAttribute("innerHTML").trim().equals(text.trim())) 
					element.click();							
			});		
			Report.logMethod("setState", Status.SUCCESSFUL);
		}catch (Throwable e) {
			Report.logMethod("setState", Status.FAILED, e.getMessage());
			throw e;
		}
		return this;
	}

	public RegisterPage setZipCode(String text) {
		try {
			write(postcode, Strings.padEnd(text, 5, '0').substring(0, 5));				
			Report.logMethod("setZipCode", Status.SUCCESSFUL);
		}catch (Throwable e) {
			Report.logMethod("setZipCode", Status.FAILED, e.getMessage());
			throw e;
		}
		return this;
	}

	public RegisterPage setCountry(Country country) {
		try {
			click(this.country);
			findElements(countries).forEach(element -> {
				if (getText(element).replace(" ", "").equals(country.name())) 
					element.click();
			});		
			Report.logMethod("setCountry", Status.SUCCESSFUL);
		}catch (Throwable e) {
			Report.logMethod("setCountry", Status.FAILED, e.getMessage());
			throw e;
		}
		return this;
	}

	public RegisterPage setNumberPhone(String text) {
		try {
			write(mobilePhone, text);				
			Report.logMethodAll("setNumberPhone", BaseTest.testName, Status.SUCCESSFUL);
		}catch (Throwable e) {
			Report.logMethodAll("setNumberPhone", BaseTest.testName, Status.FAILED, e.getMessage());
			throw e;
		}
		return this;
	}
	
	public <T extends BasePage> T clickInRegisterButton(Class<T> clazz) {	
		T page = null;
		try {			
			click(register);
			page = createPage(clazz);
			Report.logMethod("clickInRegisterButton", Status.SUCCESSFUL);
		}catch (Throwable e) {
			Report.logMethod("clickInRegisterButton", Status.FAILED);
		}
		return page;		
	}
}