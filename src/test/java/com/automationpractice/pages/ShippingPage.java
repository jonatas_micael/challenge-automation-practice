package com.automationpractice.pages;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.automationpractice.util.Item;
import com.automationpractice.util.Report;
import com.automationpractice.util.Status;
import com.google.common.base.Strings;

public class ShippingPage extends BasePage {

	private By footer = By.cssSelector(".footer-container");
	private By proceedToCheckout = By.cssSelector(".cart_navigation span");
	private By checkBox = By.id("cgv");
	private By totalShipping = By.cssSelector("div.delivery_option_price");

	public ShippingPage(WebDriver driver) {
		super(driver);
		loadPage();
	}

	@Override
	protected void loadPage() {
		wait(ExpectedConditions.visibilityOf(find(totalShipping)), DEFAULT_TIMEOUT);
	}

	public ShippingPage clickInAgreeTerms() {
		try {
			find(checkBox).click();
			Report.logMethod("clickInAgreeTerms", Status.SUCCESSFUL);
		} catch (Throwable e) {
			Report.logMethod("clickInAgreeTerms", Status.FAILED);
			throw e;
		}
		return this;
	}

	public ShippingPage validateDeliveryPrice(Item item) {
		try {
			assertEquals(normalizeString("$" + item.getShippingValue()),
					normalizeString(getText(totalShipping).trim()));
			Report.logMethod("validateDeliveryPrice", Status.SUCCESSFUL);
		} catch (Throwable e) {
			Report.logMethod("validateDeliveryPrice", Status.FAILED);
			throw e;
		}
		return this;
	}

	private String normalizeString(String string) {
		return Strings.padEnd(string, 6, '0');
	}

	public <T extends BasePage> T proceedToCheckout(Class<T> clazz) {
		T page = null;
		try {
			wait(ExpectedConditions.visibilityOf(find(proceedToCheckout)), DEFAULT_TIMEOUT);
			click(proceedToCheckout);
			page = createPage(clazz);
			Report.logMethod("proceedToCheckout", Status.SUCCESSFUL);
		} catch (Throwable e) {
			Report.logMethod("proceedToCheckout", Status.FAILED, e.getMessage());
		}
		return page;
	}

}
