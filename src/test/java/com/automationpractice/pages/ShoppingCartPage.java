package com.automationpractice.pages;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.automationpractice.tests.BaseTest;
import com.automationpractice.util.Item;
import com.automationpractice.util.Report;
import com.automationpractice.util.Status;
import com.google.common.base.Strings;

public class ShoppingCartPage extends BasePage {

	By productName = By.cssSelector("#order-detail-content .product-name a");
	By proceedToCheckout = By.cssSelector(".cart_navigation span");	
	By totalProduct = By.id("total_product");
	By totalShipping = By.id("total_shipping");
	By totalPrice = By.id("total_price");
		
	public ShoppingCartPage(WebDriver driver) {
		super(driver);		
		loadPage();
	}

	@Override
	protected void loadPage() {	
		wait(ExpectedConditions.visibilityOf(find(totalPrice)), DEFAULT_TIMEOUT);
		wait(ExpectedConditions.visibilityOf(find(proceedToCheckout)), DEFAULT_TIMEOUT);
	}

	public ShoppingCartPage validateItemData(Item selectedItem) {
		try {
			assertEquals(selectedItem.getProductName(), getText(productName));
			assertEquals(normalizeString("$" + selectedItem.getUnitValue()), normalizeString(getText(totalProduct)));
			assertEquals(normalizeString("$" + selectedItem.getShippingValue()), normalizeString(getText(totalShipping)));
			assertEquals(normalizeString(("$" + (selectedItem.getUnitValue() + selectedItem.getShippingValue()))), normalizeString(getText(totalPrice)));
		
			Report.logMethodAll("validateItemData", BaseTest.testName, Status.SUCCESSFUL);
		}catch (Throwable e) {
			Report.logMethodAll("validateItemData", BaseTest.testName, Status.FAILED, e.getMessage());
			throw e;
		}
		return this;
	}
	
	public String normalizeString(String string) {
		return Strings.padEnd(string, 6, '0');
	}

	public <T extends BasePage> T proceedToCheckout(Class<T> clazz)  {	
		T page = null;
		try {
			wait(ExpectedConditions.visibilityOf(find(proceedToCheckout)), DEFAULT_TIMEOUT);
			click(proceedToCheckout);
			page = createPage(clazz);
			Report.logMethod("proceedToCheckout", Status.SUCCESSFUL);
		}catch (Throwable e) {
			Report.logMethod("proceedToCheckout", Status.FAILED, e.getMessage());
		}
		return page;
	}

}
