package com.automationpractice.pages;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.automationpractice.tests.BaseTest;
import com.automationpractice.util.Item;
import com.automationpractice.util.Report;
import com.automationpractice.util.Status;
import com.google.common.base.Strings;

public class OrderConfirmation extends BasePage {

	private By footer = By.id("footer");
	private By price = By.cssSelector("#center_column span.price strong");

	public OrderConfirmation(WebDriver driver) {
		super(driver);
		loadPage();
	}

	@Override
	protected void loadPage() {
		wait(ExpectedConditions.visibilityOf(find(price)), DEFAULT_TIMEOUT);
	}

	public OrderConfirmation validateOrderConfirmation(Item selectedItem) {

		try {
			assertEquals(normalizeString(("$" + (selectedItem.getUnitValue() + selectedItem.getShippingValue()))),
					normalizeString(getText(price)));
			
			Report.logMethodAll("validateItemData", BaseTest.testName, Status.SUCCESSFUL);
		} catch (Throwable e) {
			Report.logMethodAll("validateItemData", BaseTest.testName, Status.FAILED, e.getMessage());
			throw e;
		}
		return this;

	}

	private String normalizeString(String string) {
		return Strings.padEnd(string, 6, '0');
	}

}
