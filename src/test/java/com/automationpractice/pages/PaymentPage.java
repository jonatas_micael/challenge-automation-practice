package com.automationpractice.pages;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.automationpractice.tests.BaseTest;
import com.automationpractice.util.Item;
import com.automationpractice.util.PaymentForms;
import com.automationpractice.util.Report;
import com.automationpractice.util.Status;
import com.google.common.base.Strings;

public class PaymentPage extends BasePage {
	
	private By productName = By.cssSelector("#order-detail-content .product-name a");
	private By paymentModule = By.className("payment_module");
	private By totalProduct = By.id("total_product");
	private By totalShipping = By.id("total_shipping");
	private By totalPrice = By.id("total_price");
	private By footer = By.id("footer");
	
	public PaymentPage(WebDriver driver) {
		super(driver);
		loadPage();
	}
	
	@Override
	protected void loadPage() {		
		wait(ExpectedConditions.visibilityOf(find(totalPrice)), DEFAULT_TIMEOUT);	
	}

	public PaymentPage validateTotalValues(Item selectedItem) {		
		try {
			assertEquals(selectedItem.getProductName(), getText(productName));
			assertEquals(normalizeString("$" + selectedItem.getUnitValue()), normalizeString(getText(totalProduct)));
			assertEquals(normalizeString("$" + selectedItem.getShippingValue()), normalizeString(getText(totalShipping)));
			assertEquals(normalizeString(("$" + (selectedItem.getUnitValue() + selectedItem.getShippingValue()))), normalizeString(getText(totalPrice)));
		
			Report.logMethodAll("validateItemData", BaseTest.testName, Status.SUCCESSFUL);
		}catch (Throwable e) {
			Report.logMethodAll("validateItemData", BaseTest.testName, Status.FAILED, e.getMessage());
			throw e;
		}
		return this;	
	}
	
	private String normalizeString(String string) {
		return Strings.padEnd(string, 6, '0');
	}
	
	public OrderSummaryPage payBy(PaymentForms paymentForm) {
		try {
			wait(ExpectedConditions.visibilityOf(findElements(paymentModule).get(1)), DEFAULT_TIMEOUT);
			findElements(paymentModule).get(paymentForm.ordinal()).click();
			Report.logMethod("proceedToCheckout", Status.SUCCESSFUL);
		} catch (Throwable e) {
			Report.logMethod("proceedToCheckout", Status.FAILED, e.getMessage());
		}
		return new OrderSummaryPage(driver);
	}
}
