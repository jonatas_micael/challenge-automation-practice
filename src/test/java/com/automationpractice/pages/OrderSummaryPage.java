package com.automationpractice.pages;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.automationpractice.tests.BaseTest;
import com.automationpractice.util.Item;
import com.automationpractice.util.Report;
import com.automationpractice.util.Status;
import com.google.common.base.Strings;

public class OrderSummaryPage extends BasePage {

	private By footer = By.id("footer");
	private By totalAmount = By.id("amount");
	private By confirmOrder = By.cssSelector(".cart_navigation span");
	
	public OrderSummaryPage(WebDriver driver) {
		super(driver);
		loadPage();
	}

	@Override
	protected void loadPage() {
		wait(ExpectedConditions.visibilityOf(find(confirmOrder)), DEFAULT_TIMEOUT);
	}

	public OrderSummaryPage validateOrderSummary(Item item) {
		try {
			assertEquals(normalizeString("$" + (item.getShippingValue() + item.getUnitValue())),
					normalizeString(getText(totalAmount)));
			Report.logMethodAll("validateOrderSummary", BaseTest.testName, Status.SUCCESSFUL);
		} catch (Throwable e) {
			Report.logMethodAll("validateOrderSummary", BaseTest.testName, Status.FAILED, e.getMessage());
			throw e;
		}
		return this;
	}
	
	private String normalizeString(String string) {
		return Strings.padEnd(string, 6, '0');
	}
	
	public <T extends BasePage> T confirmOrder(Class<T> clazz) {
		T page = null;
		try {
			wait(ExpectedConditions.visibilityOf(find(confirmOrder )), DEFAULT_TIMEOUT);
			click(confirmOrder);
			page = createPage(clazz);
			Report.logMethod("confirmOrder", Status.SUCCESSFUL);
		}catch (Throwable e) {
			Report.logMethod("confirmOrder", Status.FAILED, e.getMessage());
		}
		return page;
	}

}
