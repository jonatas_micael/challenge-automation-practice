package com.automationpractice.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.automationpractice.tests.BaseTest;
import com.automationpractice.util.Item;
import com.automationpractice.util.Report;
import com.automationpractice.util.Status;

public class MainPage extends BasePage{
	
	private By addToCard = By.cssSelector("#homefeatured .ajax_add_to_cart_button");
	private By proceedToCheckout = By.cssSelector(".button-container .button-medium");
	private By footer = By.cssSelector(".footer-container");
	
	public MainPage(WebDriver driver) {
		super(driver);
		loadPage();
	}
	
	protected void loadPage() {
		wait(ExpectedConditions.visibilityOf(find(footer)), DEFAULT_TIMEOUT);
	}

	public MainPage addItemToCart(Item selectedItem) {
		try {
			findElements(addToCard).get(Item.getIndex(selectedItem)).click();
			wait(ExpectedConditions.visibilityOf(find(proceedToCheckout)), DEFAULT_TIMEOUT);
			Report.logMethodAll("addItemToCart", BaseTest.testName, Status.SUCCESSFUL);
		}catch (Throwable e) {
			Report.logMethodAll("addItemToCart", BaseTest.testName, Status.FAILED, e.getMessage());
			throw e;
		}
		return this;
	}

	public <T extends BasePage> T proceedToCheckout(Class<T> clazz) {	
		T page = null;
		try {
			wait(ExpectedConditions.visibilityOf(find(proceedToCheckout)), DEFAULT_TIMEOUT);
			find(proceedToCheckout).click();		
			page = createPage(clazz);
			Report.logMethod("proceedToCheckout", Status.SUCCESSFUL);
		}catch (Throwable e) {
			Report.logMethod("proceedToCheckout", Status.FAILED, e.getMessage());
		}
		return page;
	}	
}
