package com.automationpractice.pages;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.automationpractice.util.Report;
import com.automationpractice.util.Status;
import com.google.common.base.Strings;

import generators.addressGenerator.Address;
import generators.peopleGenerator.Person;

//import generators.addressGenerator.Address;

public class AddressesPage extends BasePage {
	
	private By deliveryBlock = By.id("address_delivery");
	private By billingBlock = By.id("address_invoice");
	private By footer = By.id("footer");
	private By proceedToCheckout = By.cssSelector(".cart_navigation span");	
	private By names = By.cssSelector(".address_firstname.address_lastname");
	private By addressName = By.cssSelector(".address_address1.address_address2");
	private By cityStatePost = By.cssSelector(".address_city.address_state_name.address_postcode");
	private By country = By.cssSelector(".address_country_name");
	private By phoneMobile = By.cssSelector(".address_phone_mobile");	
	
	public AddressesPage(WebDriver driver) {
		super(driver);
		loadPage();
	}
	
	@Override
	protected void loadPage() {		
		wait(ExpectedConditions.visibilityOf(find(proceedToCheckout)), DEFAULT_TIMEOUT);
	}

	public AddressesPage validateAddress(Person person) {
		Address address = person.getAddress();
		try {		
			//DELIVERY ADDRESS
			//BILLING ADDRESS
			WebElement actualBlock = find(deliveryBlock);		
			for (int i = 0; i < 2; i++, actualBlock = find(billingBlock)) {
				assertEquals(person.getName().getFullName(), getVisibleText(actualBlock, names).trim()); 
				assertEquals(address.getAddressName(), getVisibleText(actualBlock, addressName).trim());
				assertEquals(address.getCity() + ", " + address.getState() + " " + Strings.padEnd(address.getZipCode(), 5, '0').substring(0, 5), getVisibleText(actualBlock, cityStatePost).trim());
				assertEquals(address.getCountry().name(), getVisibleText(actualBlock, country).replace(" ", "").trim());
				assertEquals(person.getNumberPhone(), getVisibleText(actualBlock, phoneMobile).trim());
			}				
			Report.logMethod("validateAddress", Status.SUCCESSFUL);
		}catch (Throwable e) {
			Report.logMethod("validateAddress", Status.FAILED, e.getMessage());
		}
		
		return this;
	}

	public <T extends BasePage> T proceedToCheckout(Class<T> clazz)  {	
		T page = null;
		try {
			wait(ExpectedConditions.visibilityOf(find(proceedToCheckout)), DEFAULT_TIMEOUT);
			click(proceedToCheckout);
//			page = createPage(clazz);
			page = (T) new ShippingPage(driver);
			Report.logMethod("proceedToCheckout", Status.SUCCESSFUL);
		}catch (Throwable e) {
			Report.logMethod("proceedToCheckout", Status.FAILED, e.getMessage() == null ? e.getStackTrace().toString() : e.getMessage());
		}
		return page;
	}

}
