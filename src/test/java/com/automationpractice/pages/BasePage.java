package com.automationpractice.pages;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class BasePage {
	
	protected WebDriver driver;
	protected final int DEFAULT_TIMEOUT = 10;
	protected final int SHORT_TIMEOUT = 5;
	protected final int MEDIUM_TIMEOUT = 12;
	protected final int LONG_TIMEOUT = 20;
	
	public BasePage(WebDriver driver) {
		this.driver = driver;
	}
	
	protected abstract void loadPage();
	
	protected void click(By locator) {
		find(locator).click();
	}
	
	protected void clear(By locator) {
		clear(locator, false);
	}
	
	protected void clear(By locator, boolean deleting) {
		if (deleting)
			while( !getText(locator).equals("") )
				write(locator, Keys.BACK_SPACE);
		else
			find(locator).clear();	
	}
	
	protected String getVisibleText(WebElement element, By locator) {		
		return element.findElement(locator).getAttribute("innerHTML");
	}
	
	protected String getText(WebElement element, By locator) {		
		return getText(element.findElement(locator));
	}
	
	protected String getText(By locator) {		
		return getText(find(locator));
	}
	
	protected String getText(WebElement element) {		
		return !element.getText().equals("") 
				? element.getAttribute("value") == null 
					? element.getAttribute("innerHTML") == null
						? ""
					: element.getAttribute("innerHTML")							
				: element.getAttribute("value")
			: element.getText();				
	}

	protected void write(By locator, CharSequence text) {
		write(find(locator), text);
	}
	
	protected void write(WebElement element, CharSequence text) {
		element.sendKeys(text);
	}
	
	protected WebElement find(By locator) {
		return driver.findElement(locator);
	}
	
	protected List<WebElement> findElements(By locator) {
		return driver.findElements(locator);
	}
	
	protected void wait(ExpectedCondition<?> expectedCondition, int timeOutInSeconds) {		
		new WebDriverWait(driver, timeOutInSeconds).until(expectedCondition);
	}
	
	protected <T extends BasePage> T createPage(Class<T> clazz) throws Exception{		
		return (T) clazz.getDeclaredConstructor(WebDriver.class).newInstance(driver);
	}	
}

