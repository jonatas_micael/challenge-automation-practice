package com.automationpractice.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.google.common.io.Files;

public class Report {
	
	private static WebDriver driver = null;
	private static String executionID = "";
	private static String basePath = System.getProperty("user.dir") + "\\src\\test\\resources\\reports\\Test-";
	private static File logFile = null;

	public static void setDriver(WebDriver driver) {
		Report.driver = driver;
	}
	
	public static void setExecutionID(String executionID) {
		if ( !executionID.equals(Report.executionID)) {
			new File(basePath + executionID).mkdir();
			new File(basePath + executionID + "\\screenshots").mkdir();
			try {
				logFile = new File(basePath + executionID + "\\report.txt");
				logFile.createNewFile();
			} catch (IOException e) {
				System.err.println("it was not possible to create a log file");
				e.printStackTrace();
			}
		}
		Report.executionID = executionID;
	}
			
	public static void log(String message) {	
		try {			
			FileWriter writer = new FileWriter(logFile, true);
			writer.write(message + "\n");
			writer.close();
			
		} catch (IOException e) {
			System.err.println("it was not possible open arquive");
			e.printStackTrace();
		}
	}
	
	public static void captureScreen(String testName, Status testStatus) {        	
		try {			
			((JavascriptExecutor) driver).executeScript("scroll(0, 400)");
			
			Files.move(((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE), new File(basePath + executionID + "\\screenshots\\" + testName + "_" + testStatus.name() + "_" + Calendar.getInstance().getTimeInMillis() + ".png"));
		
		} catch (IOException e) {
			System.err.println("it was not possible to capture screen");
			e.printStackTrace();
		}
	}

	public static void logAll(String action, String testName, Status testStatus) {
		log(action, testStatus);
		captureScreen(testName, testStatus);
	}
	
	
	public static void log(String action, Status testStatus) {	
		log(action + " - " + testStatus.name());	
	}
	
	public static void log(String action, Status testStatus, String message) {		
		log(action + " - " + testStatus.name() + " - " + message);	
	}	

	public static void logAll(String action, String testName, Status testStatus, String message) {
		log(action, testStatus, message);
		captureScreen(testName, testStatus);
		
	}
	public static void logTestName(String testName) {
		log("TestName: " + testName);
	}
	
	public static void logMethod(String methodName, Status status) {
		log("MethodName: " + methodName, status);
	}

	public static void logMethodAll(String methodName, String testName, Status status) {
		logAll("MethodName: " + methodName, testName, status);
	}

	public static void logMethodAll(String action, String testName, Status testStatus, String message) {
		logAll("MethodName: " + action, testName, testStatus, message);		
	}

	public static void logMethod(String methodName, Status testStatus, String message) {
		log("MethodName: " + methodName + " - " + testStatus.name() + " - " + message);		
	}
}
