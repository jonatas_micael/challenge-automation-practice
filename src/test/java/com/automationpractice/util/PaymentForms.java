package com.automationpractice.util;

public enum PaymentForms {
	BankWire,
	Check;
}
