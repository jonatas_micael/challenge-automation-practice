package com.automationpractice.util;

import java.util.Arrays;

public enum Item {
	
	Model_1("Faded Short Sleeve T-shirts", 16.51, 2.00),
	Model_2("Blouse", 27.00, 2.00),
	Model_3("Printed Dress", 26.00, 2.00),
	Model_4("Printed Dress", 50.99, 2.00),
	Model_5("Printed Summer Dress", 28.98, 2.00),
	Model_6("Printed Summer Dress", 30.50, 2.00),
	Model_7("Printed Chiffon Dress", 16.40, 2.00);
	
	String productName;
	double unitValue;
	double shippingValue;
	
	private Item(String productName, double unitValue, double shippingValue) {
		this.productName = productName;
		this.unitValue = unitValue;		
		this.shippingValue = shippingValue;
	}
	
	public String getProductName() {
		return productName;
	}
	
	public double getShippingValue() {
		return shippingValue;
	}
	
	public double getUnitValue() {
		return unitValue;
	}
	
	public static int getIndex(Item item) {		
		return Arrays.asList((Item.values())).indexOf(item);		
	}
	
}
